#!/bin/bash
purgecss --css deployment/css/*.css --content views/*.erb --out deployment/css/purged/
cat deployment/css/purged/*.css > deployment/css/combined/combined.css
cleancss -o public/static/css/combined.min.css deployment/css/combined/combined.css
