# frozen_string_literal: true

# Helper class for redis cache
class Insomnia247Nl < Sinatra::Base
  require 'redis'
  before do
    @redis = Redis.new(path: settings.redis_socket)
    @redis.ping
  rescue Redis::CannotConnectError
    @redis = nil
  end

  # rubocop:disable Metrics/BlockLength
  helpers do
    def cache_get(key)
      return nil if @redis.nil?

      @redis.get(cache_prefix(key))
    end

    def cache_set(key, content)
      return nil if @redis.nil?

      @redis.set(cache_prefix(key), content)
    end

    def cache_count
      return nil if @redis.nil?

      cache_keys.count.to_s
    end

    def cache_drop(key)
      return nil if @redis.nil?

      cache_safe_drop(cache_prefix(key))
      'OK'
    end

    def cache_undrop(key)
      return nil if @redis.nil?

      cache_safe_undrop(cache_prefix(key))
      'OK'
    end

    def cache_flush
      return nil if @redis.nil?

      cache_safe_flush
      'OK'
    end

    def cache_unflush
      return nil if @redis.nil?

      cache_safe_unflush
      'OK'
    end
  end
  # rubocop:enable Metrics/BlockLength

  private

  def cache_prefix(key)
    "#{settings.redis_prefix}#{key}"
  end

  def cache_keys
    @redis.keys(cache_prefix('*'))
  end

  def cache_safe_drop(key)
    @redis.set("#{key}-flushed", @redis.get(key))
    @redis.del(key)
  end

  def cache_safe_undrop(key)
    @redis.set(key, @redis.get("#{key}-flushed"))
  end

  def cache_safe_flush
    cache_keys.each do |key|
      cache_safe_drop key unless key.end_with? '-flushed'
    end
  end

  def cache_safe_unflush
    cache_keys.each do |key|
      cache_safe_undrop(key.chomp('-flushed')) if key.end_with? '-flushed'
    end
  end
end
