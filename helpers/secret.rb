# frozen_string_literal: true

# Helper class to check secret
class Insomnia247Nl < Sinatra::Base
  helpers do
    def check_secret(params)
      halt 500 unless ENV.key?('SECRET')
      halt 401 unless params.key?('secret')
      halt 401 unless params['secret'] == ENV['SECRET']
    end
  end
end
