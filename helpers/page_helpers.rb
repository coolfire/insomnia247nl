# frozen_string_literal: true

# Helper class to generate common page sections
class Insomnia247Nl < Sinatra::Base
  helpers do
    def make_page(title, content, cache = true)
      menu    = make_menu
      meta    = getmeta(title)
      content = erb :base, locals: {
        title: title,
        meta: meta,
        menu: menu,
        content: content
      }
      cache_set(title, content) if cache

      content
    end

    def make_menu
      menu = cache_get('menu')
      return menu unless menu.nil?

      items = YAML.load_file("#{settings.content_prefix}/menu.yml")
      menu  = erb :menu, locals: { items: items }
      cache_set('menu', menu)

      menu
    end

    def getmeta(title)
      metadata = YAML.load_file("#{settings.content_prefix}/meta.yml")
      return metadata['News'] unless metadata.key? title

      metadata[title]
    end
  end
end
