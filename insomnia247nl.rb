# frozen_string_literal: true

require 'sinatra/base'
require 'sinatra/config_file'

# Main class
class Insomnia247Nl < Sinatra::Base
  register Sinatra::ConfigFile
  config_file 'config.yml'

  set :static, true
  set :static_cache_control, [:public, { max_age: 86_400 }]
  set :erb, trim: '-'

  before do
    expires 3600, :public, max_age: 3600
  end

  get '/' do
    redirect '/news'
  end

  require_relative 'helpers/page_helpers'
  require_relative 'helpers/redis_cache'
  require_relative 'helpers/secret'
  require_relative 'routes/cache'
  require_relative 'routes/contact'
  require_relative 'routes/donate'
  require_relative 'routes/hashdb'
  require_relative 'routes/images'
  require_relative 'routes/legacy_redirects'
  require_relative 'routes/lostpass'
  require_relative 'routes/news'
  require_relative 'routes/projects'
  require_relative 'routes/suspended_domain'
  require_relative 'routes/tagline'
end
