# frozen_string_literal: true

# Hashdb page
class Insomnia247Nl < Sinatra::Base
  get '/hashdb' do
    title = 'Hash database search'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :hashdb, locals: { message: nil }
    make_page(title, content)
  end

  post '/hashdb' do
    title = 'Hash database search'

    unless params.key? 'hash'
      content = erb :hashdb, locals: { message: 'No hash submitted', type: 'danger' }
      return make_page(title, content, false)
    end

    result  = check_hash(params['hash'])
    content = erb :hashdb, locals: { message: result[0], type: result[1] }
    make_page(title, content, false)
  end

  private

  def check_hash(hash)
    return ['No hash submitted', 'danger'] if hash.empty?

    type = { 32 => 'md5', 40 => 'sha1', 64 => 'sha256' }[hash.length]
    return ['Unsupported hash type', 'danger'] if type.nil?

    return ['Unsupported hash type', 'danger'] if /^[0-9a-f]+$/.match(hash).nil?

    search_hashdb(hash, type)
  end

  def search_hashdb(hash, type)
    require 'net/http'
    uri          = URI.parse("https://hashdb.insomnia247.nl/v1/#{type}/#{hash}")
    http         = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request      = Net::HTTP::Get.new(uri.request_uri)
    response     = http.request(request)
    result       = JSON.parse(response.body)

    return ['The hash was not found our database.', 'primary'] unless result['found']

    ["Hash found!<br />Result;<br /><pre>#{CGI.escape_html(result['result'])}</pre>", 'success']
  end
end
