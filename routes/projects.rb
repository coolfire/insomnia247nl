# frozen_string_literal: true

# Projects page
class Insomnia247Nl < Sinatra::Base
  get '/projects' do
    title = 'Projects'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :projects, locals: { items: YAML.load_file("#{settings.content_prefix}/projects.yml") }
    make_page(title, content)
  end
end
