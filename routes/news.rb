# frozen_string_literal: true

# News page
class Insomnia247Nl < Sinatra::Base
  get '/news' do
    title = 'News'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :news, locals: { items: YAML.load_file("#{settings.content_prefix}/news.yml") }
    make_page(title, content)
  end
end
