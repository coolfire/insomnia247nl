# frozen_string_literal: true

# Cache control functions
class Insomnia247Nl < Sinatra::Base
  get '/cache/status' do
    check_secret params
    cache_count
  end

  get '/cache/drop/:key' do
    check_secret params
    cache_drop params['key']
  end

  get '/cache/undrop/:key' do
    check_secret params
    cache_undrop params['key']
  end

  get '/cache/flush' do
    check_secret params
    cache_flush
  end

  get '/cache/unflush' do
    check_secret params
    cache_unflush
  end
end
