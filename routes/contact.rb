# frozen_string_literal: true

# Chat page
class Insomnia247Nl < Sinatra::Base
  get '/chat' do
    redirect '/contact'
  end

  get '/contact' do
    title = 'Contact'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :contact
    make_page(title, content)
  end
end
