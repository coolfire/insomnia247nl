# frozen_string_literal: true

# Donations page
class Insomnia247Nl < Sinatra::Base
  get '/donate' do
    title = 'Donate'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :news, locals: { items: YAML.load_file("#{settings.content_prefix}/donate.yml") }
    make_page(title, content)
  end
end
