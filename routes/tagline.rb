# frozen_string_literal: true

# Taglines endpoint
class Insomnia247Nl < Sinatra::Base
  get '/tagline' do
    fetch_tagline
  end

  get '/tagline-frame' do
    "<style>
      body{font-family:Verdana,Ariel,sans-serif;font-size:.9rem;color:white;text-align:center}
    </style>
    <div id='tagline'>#{fetch_tagline}"
  end

  private

  def fetch_tagline
    expires 10, :public, max_age: 10
    taglines = cache_get('taglines')
    return YAML.safe_load(taglines).sample unless taglines.nil?

    taglines = YAML.load_file("#{settings.content_prefix}/taglines.yml")['taglines']
    cache_set('taglines', taglines.to_yaml)
    taglines.sample
  end
end
