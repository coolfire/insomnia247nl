# frozen_string_literal: true

# Legacy page redirections
class Insomnia247Nl < Sinatra::Base
  get '/page/main' do
    redirect '/news'
  end

  get '/page/shells/index' do
    redirect 'https://signup.insomnia247.nl/'
  end

  get '/page/:pagename' do
    redirect "/#{cleanup params['pagename']}"
  end

  get '/page/livetools/:pagename/index' do
    redirect "/#{cleanup params['pagename']}"
  end

  get '/page/crowd/:pagename/index' do
    redirect "/#{cleanup params['pagename']}"
  end

  get '/phpmyadmin' do
    redirect 'https://adminer.insomnia247.nl/'
  end

  get '/~:user' do
    redirect "https://#{cleanup params['user']}.insomnia247.nl/"
  end

  private

  def cleanup(input)
    input.gsub(/[^0-9a-zA-Z\-_]/, '')
  end
end
