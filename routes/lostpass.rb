# frozen_string_literal: true

# lostpass.html endpoints
class Insomnia247Nl < Sinatra::Base
  get '/lostpass' do
    redirect 'https://wiki.insomnia247.nl/wiki/Shells_FAQ#I_lost_my_password.2C_what_should_I_do.3F'
  end

  get '/lostpass.html' do
    redirect 'https://wiki.insomnia247.nl/wiki/Shells_FAQ#I_lost_my_password.2C_what_should_I_do.3F'
  end
end
