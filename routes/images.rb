# frozen_string_literal: true

# Images page
class Insomnia247Nl < Sinatra::Base
  require 'mini_magick'
  get '/images/resized.php' do
    resized_header params
  end

  get '/images/resized.png' do
    resized_header params
  end

  get '/images/resized/:w' do
    resized_header params
  end

  private

  # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
  def resized_header(params)
    w = 437
    h = 120

    if params.key? 'w'
      halt 400 if params['w'].to_i > 1000
      w = params['w'].to_i
      h = w * 0.274599542
    end

    output = cache_get "header-#{w}.png"
    if output.nil?
      image = MiniMagick::Image.open('public/static/images/header.png')
      image.resize "#{w}x#{h}"
      image.format 'png'
      image.write "tmp/header-#{w}.png"
      output = File.read "tmp/header-#{w}.png"
      File.delete "tmp/header-#{w}.png"
      cache_set "header-#{w}.png", output
    end

    content_type :png
    output
  end
  # rubocop:enable Metrics/AbcSize, Metrics/MethodLength
end
