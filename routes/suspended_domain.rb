# frozen_string_literal: true

# Suspended domain page
class Insomnia247Nl < Sinatra::Base
  get '/suspended' do
    redirect '/suspended-domain'
  end

  get '/suspended-domain' do
    title = 'Suspended domain'
    page  = cache_get(title)
    return page unless page.nil?

    content = erb :news, locals: { items: YAML.load_file("#{settings.content_prefix}/suspended_domain.yml") }
    make_page(title, content)
  end
end
